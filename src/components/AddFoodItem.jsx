import React from 'react';

class AddFoodItem extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'AddFoodItem';
    }

    render() {
        return (
          <div className="col-md-6 col-lg-6 col-sm-12">
            <div className="page-header">
              <h2>Create New Food</h2>
            </div>
            <form>
              <div className="panel panel-default">
                <div className="panel-heading">
                  General information
                </div>
                <div className="panel-body">
                  <div className="form-group row">
                    <div className="col-xs-6">
                      <label htmlFor="item-name">Name</label>
                      <input type="text" className="form-control" name="item-name"/>
                    </div>
                  </div>
                  <div className="form-group row">
                    <div className="col-xs-6">
                      <label htmlFor="item-brand">Brand</label>
                      <input type="text" className="form-control" name="item-brand"/>
                    </div>
                  </div>
                </div>
              </div>
              <div className="panel panel-default">
                <div className="panel-heading">
                  Nutritional information
                </div>
                <div className="panel-body">
                  <div className="row">
                    <div className="form-group col-xs-3">
                        <label htmlFor="item-calories">Calories</label>
                        <input type="text" className="form-control" name="item-calories"/>
                    </div>
                    <div className="form-group col-xs-3">
                        <label htmlFor="item-protein">Protein</label>
                        <input type="text" className="form-control" name="item-protein"/>
                    </div>
                  </div>
                  <div className="row">
                    <div className="form-group col-xs-3">
                        <label htmlFor="item-carbohydrates">Carbohydrates</label>
                        <input type="text" className="form-control" name="item-carbohydrates"/>
                    </div>
                    <div className="form-group col-xs-3">
                        <label htmlFor="item-fat">Fat</label>
                        <input type="text" className="form-control" name="item-fat"/>
                    </div>
                  </div>
                </div>
              </div>
              <button className="btn btn-success" type="submit">
                <span className="glyphicon glyphicon-plus"></span> Create Food
              </button>
            </form>
          </div>
        );
    }
}

export default AddFoodItem;
