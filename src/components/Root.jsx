import React from 'react';
import AddFoodItem from './AddFoodItem'

class Root extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Root';
    }

    render() {
        return (
            <div className="row">
              <AddFoodItem />
            </div>
        );
    }
}

export default Root;
